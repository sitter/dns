DOMAIN=$1

echo
echo "** Generating the keys for $DOMAIN"
cd /etc/bind/keys
dnssec-keygen -r/dev/urandom -a RSASHA256 -b 2048 -3 $DOMAIN 
dnssec-keygen -r/dev/urandom -a RSASHA256 -b 2048 -3 -fk $DOMAIN
chown bind:bind *

echo
echo "** Signing the zone file"
NSECSEED=$(printf "%04x%04x" $RANDOM $RANDOM)
rndc signing -nsec3param 1 0 10 $NSECSEED $DOMAIN 

echo
echo "**Reloading bind"
/etc/init.d/bind9 reload

sleep 5

echo
echo "** Please bump SOA serial and let it spread around the world"

echo
echo "** Upload the record (second line only is fine) to Namecheap"
echo "    dig @localhost dnskey $DOMAIN | dnssec-dsfromkey -f - $DOMAIN"
dig @localhost dnskey $DOMAIN | dnssec-dsfromkey -f - $DOMAIN 
cd /root

echo
echo "** Check it at https://dnssec-analyzer.verisignlabs.com/$DOMAIN"
