#!/bin/bash

# Switch to our DNS checkout and update it
cd /srv/dns/
git pull -q

# Copy over our Bind configuration
cp bind/* /etc/bind/
systemctl reload bind9

# Deploy our Zones now. We track the ones which have changed to trigger a DNS change
for zoneToDeploy in zones/*.zone; do
    # Determine which zone we are updating
    domain=`basename $zoneToDeploy .zone`

    # What will the new path be?
    newPath="/etc/bind/master/$domain.zone"

    # Has the zone changed / is it new?
    if [[ ! -e $newPath ]] || ! cmp -s $zoneToDeploy $newPath; then
        # Deploy it
        cp $zoneToDeploy $newPath
	/usr/sbin/rndc reload $domain
    fi
done
