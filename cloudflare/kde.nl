;;
;; Domain:     kde.nl.
;; Exported:   2019-10-12 16:45:28
;;
;; This file is intended for use for informational and archival
;; purposes ONLY and MUST be edited before use on a production
;; DNS server.  In particular, you must:
;;   -- update the SOA record with the correct authoritative name server
;;   -- update the SOA record with the contact e-mail address information
;;   -- update the NS record(s) with the authoritative name servers for this domain.
;;
;; For further information, please consult the BIND documentation
;; located on the following website:
;;
;; http://www.isc.org/
;;
;; And RFC 1035:
;;
;; http://www.ietf.org/rfc/rfc1035.txt
;;
;; Please note that we do NOT offer technical support for any use
;; of this zone data, the BIND name server, or any other third-party
;; DNS software.
;;
;; Use at your own risk.

;; SOA Record
kde.nl.	3600	IN	SOA	kde.nl. root.kde.nl. 2032256532 7200 3600 86400 3600

;; CNAME Records
forum.kde.nl.	1	IN	CNAME	edulis.kde.org.
kde.nl.	1	IN	CNAME	edulis.kde.org.
www.kde.nl.	1	IN	CNAME	edulis.kde.org.

;; MX Records
kde.nl.	1	IN	MX	10 letterbox.kde.org.